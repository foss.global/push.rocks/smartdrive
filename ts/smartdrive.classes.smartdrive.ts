import * as plugins from './smartdrive.plugins';
import * as paths from './smartdrive.paths';
import * as interfaces from './smartdrive.interfaces';

export class SmartDrive {
  private smartshellInstance = new plugins.smartshell.Smartshell({
    executor: 'bash'
  });

  public async getLocalDriveList(): Promise<interfaces.IDrive[]> {
    const list = await plugins.drivelist.list();
    const lsblkExecResult = await this.smartshellInstance.execSilent(`lsblk --json --bytes`);
    const lsblkJson: interfaces.ILsBlkJson = JSON.parse(lsblkExecResult.stdout);
    console.log(lsblkJson);
    const extendedList: interfaces.IDrive[] = list.map(driveItemArg => {
      const deviceName = driveItemArg.device.replace('/dev/', '');
      const blockDeviceLsBlk = lsblkJson.blockdevices.find((deviceArg) => {
        return deviceArg.name === deviceName;
      })
      const result: interfaces.IDrive = {
        ...driveItemArg,
        children: blockDeviceLsBlk.children,
      }
      return result;
    });
    return extendedList;
  };

  /**
   * tries to mount a device to the corresponding name in /mnt/smartdrive/${devicename}
   * @param devNameArg 
   */
  public async mountDeviceByName(devNameArg: string) {
    await this.smartshellInstance.exec(`mount -o dmask=000,fmask=111,user /dev/sdb1 /mnt/sdb1`);
  }
}

import * as plugins from './smartdrive.plugins';
import * as interfaces from './smartdrive.interfaces';

export class DriveListResult {
  result: interfaces.IDrive;

  /**
   * tries to determine the best mounting candidate from a physical drive
   */
  public getBestMountCandidate() {}
}